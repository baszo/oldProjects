/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;
import java.net.*;
import java.util.*;
 
public class DayTimeServer extends Thread {
 
    protected DatagramSocket socket = null;
    protected BufferedReader in = null;
    protected boolean moreQuotes = true;
    protected int type = 0;
 
    public DayTimeServer() throws IOException {
    this("serverudp");
    }
 
    public DayTimeServer(String name) throws IOException {
        super(name);
        if(name.equals("UDP"))
            type=0;
        else if(name.equals("TCP"))
            type=1;
        else
            type =0;
            
        
        
        socket = new DatagramSocket(4445);
    }
 
    public void run() {
        
        //TCP
        if(type == 1){
            System.out.println("Jest polaczenie: "); 
            	BufferedReader inp = null; 
                Socket sock = null; 
                ServerSocket serv = null;  
            		   try{   
                                                   
	      serv=new ServerSocket(Integer.parseInt("4445"));      
	      String zwrotna = "";
	                                                                             
	      //oczekiwanie na polaczenie i tworzenie gniazda sieciowego                                        
    

 
                                                                
	      //tworzenie strumienia danych pobieranych z gniazda sieciowego
	      while(true) 
	      {
	    	  
		      sock=serv.accept();                                                    
		      System.out.println("Jest polaczenie: "+sock);  
  
                                                
                        zwrotna = new Date().toString();


		      
		    		  
		      OutputStream os = sock.getOutputStream();
		 	 OutputStreamWriter osw = new OutputStreamWriter(os);
		 	 BufferedWriter bw = new BufferedWriter(osw);
              bw.write(zwrotna);
              bw.flush();
              sock.close(); 
	      }
              	   }
	   catch (Exception e) 
	   {
	       e.printStackTrace();
	   }
	   finally
	   {
	       try
	       {
	 	      //zamykanie polaczenia                                                 
	 	      inp.close();                                                           
	 	      sock.close();                                                          
	 	      serv.close(); 
	       }
	       catch(Exception e){}
	   }
	        
        }
        //UDP
        else{
 
            while (moreQuotes) {
                try {
                    byte[] buf = new byte[256];

                    // receive request
                    DatagramPacket packet = new DatagramPacket(buf, buf.length);
                    socket.receive(packet);



                    String dString = null;
                    dString = new Date().toString();

                    buf = dString.getBytes();
            // send the response to the client at "address" and "port"
                    InetAddress address = packet.getAddress();
                    int port = packet.getPort();
                    packet = new DatagramPacket(buf, buf.length, address, port);
                    socket.send(packet);
                } catch (IOException e) {
            moreQuotes = false;
                }
            }
            socket.close();
        }
    }
    
     public static void main(String[] args) throws IOException {
         DayTimeServer  tmp = null;
         
         if(args.length !=1 ){
             System.out.println("Wywołanie programu z 1 argumentem UDP/TCP");
             System.exit(MIN_PRIORITY);
         }
         
         if(args[0].toString().equals("UDP"))
         {
             tmp = new DayTimeServer("UDP");
         }
         if(args[0].toString().equals("TCP"))
         {
            tmp = new DayTimeServer("TCP");
         }
         
         
         tmp.run();
    }
}



