// FTP Client

import java.net.*;
import java.io.*;
import java.util.*;


class FTPClient
{
    public static void main(String args[]) throws Exception
    {
        Socket soc=new Socket("127.0.0.1",5217);
        transferfileClient t=new transferfileClient(soc);
        t.displayMenu();
        
    }
}
class transferfileClient
{
    Socket ClientSoc;

    DataInputStream din;
    DataOutputStream dout;
    BufferedReader br;
    transferfileClient(Socket soc)
    {
        try
        {
            ClientSoc=soc;
            din=new DataInputStream(ClientSoc.getInputStream());
            dout=new DataOutputStream(ClientSoc.getOutputStream());
            br=new BufferedReader(new InputStreamReader(System.in));
        }
        catch(Exception ex)
        {
        }        
    }
    
    void ReceiveFile() throws Exception
    {
        String fileName;
        System.out.print("Podaj nazwe pliku :");
        fileName=br.readLine();
        dout.writeUTF(fileName);
        String msgFromServer=din.readUTF();
        
        if(msgFromServer.compareTo("File Not Found")==0)
        {
            System.out.println("Brak podanego pliku na serwerze ...");
            return;
        }
        else if(msgFromServer.compareTo("READY")==0)
        {
            
            int port = Integer.parseInt(din.readUTF());
            System.out.println("Odbieram plik  ... port = " + port);
            
            
            
            
            File f=new File(fileName);
            if(f.exists())
            {
                String Option;
                System.out.println("Plik juz istnieje. Nadpisac (Y/N) ?");
                Option=br.readLine();            
                if(Option=="N")    
                {
                    dout.flush();
                    return;    
                }                
            }
            
            Socket filesoc = new Socket("127.0.0.1",port);
            DataInputStream dinfile=new DataInputStream(filesoc.getInputStream());
            
            FileOutputStream fout=new FileOutputStream(f);
            int ch;
            String temp;
            do
            {
                
                temp=dinfile.readUTF();
                
//                temp=din.readUTF();
                
                ch=Integer.parseInt(temp);
                if(ch!=-1)
                {
                    fout.write(ch);                    
                }
            }while(ch!=-1);
            fout.close();
            System.out.println(din.readUTF());
            filesoc.close();
                
        }
        
        
    }

    public void displayMenu() throws Exception
    {
        while(true)
        {    
            System.out.println("[ MENU ]");
            System.out.println("1. Pobierz Plik");
            System.out.println("2. List File");
            System.out.println("3. Exit");
            System.out.print("\nWybierz :");
            int choice;
            choice=Integer.parseInt(br.readLine());
            if(choice==1)
            {
                dout.writeUTF("GET");
                ReceiveFile();
            }
            else if(choice==2)
            {
                dout.writeUTF("LIST");
                System.out.println(din.readUTF());
            }
            else
            {
                dout.writeUTF("DISCONNECT");
                System.exit(1);
            }
        }
    }
}