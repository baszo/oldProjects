// FTP Server

import java.net.*;
import java.io.*;
import java.util.*;

public class FTPServer
{
    public static void main(String args[]) throws Exception
    {
        ServerSocket soc=new ServerSocket(5217);
        System.out.println("FTP Server Dziala na Porcie  5217");
        while(true)
        {
            System.out.println("Czekam na polaczenie ...");
            transferfile t=new transferfile(soc.accept());
            
        }
    }
}

class transferfile extends Thread
{
    Socket ClientSoc;
    
    ServerSocket FileTransfer;
    Socket FileTransferSocket;

    DataInputStream din;
    DataOutputStream dout;
    
    transferfile(Socket soc)
    {
        try
        {
            ClientSoc=soc;                        
            din=new DataInputStream(ClientSoc.getInputStream());
            dout=new DataOutputStream(ClientSoc.getOutputStream());
            System.out.println("FTP Client polaczony ...");
            start();
            
        }
        catch(Exception ex)
        {
        }        
    }
    void SendFile() throws Exception
    {        
        String filename=din.readUTF();
        File f=new File(filename);
        if(!f.exists())
        {
            dout.writeUTF("File Not Found");
            return;
        }
        else
        {
            dout.writeUTF("READY");
            
            FileTransfer = new ServerSocket(3636);

            dout.writeUTF("3636");
            FileTransferSocket = FileTransfer.accept();
            DataOutputStream FileTransferdout = new DataOutputStream(FileTransferSocket.getOutputStream());
            
            
            
            FileInputStream fin=new FileInputStream(f);
            int ch;
            do
            {
                ch=fin.read();
                
//                dout.writeUTF(String.valueOf(ch));
                
                FileTransferdout.writeUTF(String.valueOf(ch));
            }
            while(ch!=-1);    
            fin.close();    
            dout.writeUTF("Plik wyslany poprawnie");  
            
            FileTransferdout.close();
            
        }
    }
 
    
    void ListFiles() throws IOException{
        String list = null;
        
          String path = "."; 
 
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles(); 

        for (int i = 0; i < listOfFiles.length; i++) 
        {

         if (listOfFiles[i].isFile()) 
         {
            list += listOfFiles[i].getName()+ "\n";
           }
        }
        
        dout.writeUTF(list);
        
        
    }


    public void run()
    {
        while(true)
        {
            try
            {
            System.out.println("czekam na polecenie ...");
            String Command=din.readUTF();
            if(Command.compareTo("GET")==0)
            {
                System.out.println("\tGET Command odebrano ...");
                SendFile();
                continue;
            }
            else if(Command.compareTo("DISCONNECT")==0)
            {
                System.out.println("\tDisconnect Command odebrano ...");
                System.exit(1);
            }
            else if(Command.compareTo("LIST")==0){
                System.out.println("\tList Command odebrano ...");
                ListFiles();
            }
            }
            catch(Exception ex)
            {
            }
        }
    }
}