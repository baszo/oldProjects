import java.io.*;
import static java.lang.Thread.sleep;
import java.net.*;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

class gameClient 
{
	public static void main(String args[]) throws Exception
	{
		int port = 0;
		if(args.length < 2)
		{
			System.err.println("Error: Podaj nazwe/ip serwera i port");
			System.exit(1);
		}
		try
		{
			port = Integer.parseInt(args[1]);
		}
		catch(NumberFormatException e)
		{
			System.err.println("Error: nie można sparsować portu na liczbe prosze podac poprawny argument");
			System.exit(1);
		}
		ClientGame game = new ClientGame(args[0], port);
		game.start();
                

	}

}

class ClientGame {
	ClientNetWorkIO IO;
        ClientNetWorkIO IOChat;
	BufferedReader inFromUser;
	public String username;
	boolean activeGame;
	ClientGame(String address, int port)
	{
		IO = new ClientNetWorkIO(address, port);
                IOChat = new ClientNetWorkIO(address, port);
		inFromUser = new BufferedReader(new InputStreamReader(System.in));

	}

	void start()
	{
		username = CreateName();
		System.out.println("Nazwa użytkownika: "+username);
		IO.sendMessage(username+":join:"+IOChat.localport);
                String response = IO.getMessage();
                
		if(response.compareTo("Serverfull") == 0)
		{
			System.out.println("Serwer zajęty, sprunuj ponownie później");
			System.exit(0);
		}
                else if(response.compareTo("EXIST") == 0)
                {
                   	System.out.println("Podany użytkownik już istnieje");
			System.exit(0); 
                }
		else
		{
                                    Runnable r = new Runnable() {
                public void run() {
                    try {
                        runYourBackgroundTaskHere();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ClientGame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                };
                new Thread(r).start();
                
			startGame();
		}
		IO.sendMessage(username+":quit");
                

	}
        public void runYourBackgroundTaskHere() throws InterruptedException{
            while(true)
            {
                String massage = IOChat.getMessage();
                if(massage.substring(0,massage.indexOf(':')).compareTo("server") == 0){
                    System.out.println("wiadomosc od serwera: "+massage.substring(massage.indexOf(':')+1));
                }
                else
                {
                    System.out.println("wiadomosc od przeciwnika: "+massage.substring(massage.indexOf(':')+1));
                }
            }
        }

	private String CreateName()
	{
		while(true)
		{
			System.out.print("Podaj nazwe użytkownika: ");
			try
			{
				return inFromUser.readLine();
			}
			catch(IOException e)
			{
				System.err.println("Error: Niepoprawne dane spróbuj ponownie");
			}
		}
	}

	private void startGame()
	{
			activeGame = true;
                        String auto = "n";
                        System.out.println("Podaj t aby automatycznie umieścić statki inaczej n");
                        try{
			auto = inFromUser.readLine();
                        }
                        catch(Exception e){
                            
                        }
                        if(auto.compareTo("t") == 0)
                        {
                            
                            IO.sendMessage(username+":"+placeShipAuto());
                        }
                        else{
			IO.sendMessage(username+":"+getShips());
                        }
			ProcessCommand(IO.getMessage());
			while(activeGame)
			{
				IO.sendMessage(username+":"+getLine());
//				System.out.println("sent package. waiting for replay...");
				ProcessCommand(IO.getMessage());
			}
                        System.exit(0);
	}

	private String getLine()
	{
		String command = "";
		while(true)
		{
			try
			{
				System.out.println("Podaj litere i numer pola");
				command = inFromUser.readLine();
			}
			catch(IOException e)
			{
				System.err.println("Error:Invalid text entry");
			}
//                        String checkifchat = command.substring(0, 2);
                        if(command.length()>2 && command.substring(0, 2).compareTo(">>") == 0)
                        {
//                            System.out.println("Wiadomośc do przeciwnika "+  command.substring(2));
                            IO.sendMessage(username+":chat:"+command.substring(2));
                            continue;
                        }
                        else if(command.compareTo("quit") == 0)
                        {
                            IO.sendMessage(username+":quit");
                            System.exit(0); 
                        }
                        
			if(command.matches("[a-jA-J]{1}[0-9]{1}"))
			{
				return command.toLowerCase();
			}
			System.out.println("Sprubuj ponownie!");
		}
	}
        
        private String placeShipAuto(){
            int size =10;
            int length = 0;
            String[] ships = new String[5];
            int currentShip = 0;
            char[][] board = new char[10][10];
            for(int i=0; i < 10; i++)
            {
                Arrays.fill(board[i], '-');
            }
            while(currentShip != 5)
		{		
			switch(currentShip)
			{
				case 0:
					 length = 5; break;
				case 1:
					 length = 4; break;
				case 2:
					 length = 3; break;
				case 3:
					 length = 3; break;
				case 4:
					 length = 2; break;
			}
                        
                        Random r = new Random();
                        int randomRow = (int) (Math.random() * (size - 1));
                        int randomCol = (int) (Math.random() * (size - 1));
                        boolean horizontal = r.nextBoolean();

                        String alphabet = "abcdefghij";
                        String shipLoc =  alphabet.charAt(randomCol)+Integer.toString(randomRow);
                        
                        if(checkLoc(shipLoc, board))
			{
				continue;
			}
                        
                        String endLoc;
                        if(horizontal){
                            if(randomCol+length-1 >= alphabet.length())
                                continue;
                            endLoc = alphabet.charAt(randomCol+length-1)+Integer.toString(randomRow);
                        }
                        else{
                            if(randomRow+length-1 >= alphabet.length())
                                continue;
                            endLoc = alphabet.charAt(randomCol)+Integer.toString(randomRow+length-1);
                        }
                        if(checkLoc(endLoc, board))
			{
				continue;
			}
                        System.out.println(shipLoc + " ----  " + endLoc);
                        
                        if(shipLoc.charAt(0) == endLoc.charAt(0))
                            {
                                    if(shipLoc.charAt(1) - endLoc.charAt(1) < 0)
                                    {
                                            if(Math.abs(shipLoc.charAt(1) - endLoc.charAt(1)-1) != length)
                                            {
                                                    continue;
                                            }
                                            for(int j = 0; j < length-2; j++)
                                            {
                                                    shipLoc += shipLoc.charAt(0);
                                                    shipLoc += (char)(shipLoc.charAt(1)+j+1);
                                            }
                                    }
                                    else
                                    {
                                            if(Math.abs(shipLoc.charAt(1) - endLoc.charAt(1)+1) != length)
                                            {
                                                    continue;
                                            }
                                            for(int j = 0; j < length-2; j++)
                                            {
                                                    shipLoc += shipLoc.charAt(0);
                                                    shipLoc += (char)(endLoc.charAt(1)+j+1);
                                            }
                                    }
                                    shipLoc += endLoc;
                                    if(isColliding(shipLoc, board))
                                    {
                                            continue;
                                    }
                                    ships[currentShip] = shipLoc;
                                    for(int i = 0; i < shipLoc.length(); i+=2)
                                    {
                                            String loc = shipLoc.substring(i, i+2);
                                            board[loc.charAt(0)-97][loc.charAt(1)-48] = 'S';
                                    }
                                    currentShip++;
                                    continue;

                            }
                            if(shipLoc.charAt(1) == endLoc.charAt(1))
                            {
                                    if(shipLoc.charAt(0) - endLoc.charAt(0) < 0)
                                    {
                                            if(Math.abs(shipLoc.charAt(0) - endLoc.charAt(0)-1) != length)
                                            {
                                                    continue;
                                            }
                                            for(int j = 0; j < length-2; j++)
                                            {
                                                    shipLoc += (char)(shipLoc.charAt(0)+j+1); 
                                                    shipLoc += shipLoc.charAt(1);
                                            }
                                    }
                                    else
                                    {
                                            if(Math.abs(shipLoc.charAt(0) - endLoc.charAt(0)+1) != length)
                                            {
                                                    continue;
                                            }
                                            for(int j = 0; j < length-2; j++)
                                            {
                                                    shipLoc += (char)(endLoc.charAt(0)+j+1);
                                                    shipLoc += shipLoc.charAt(1);
                                            }
                                    }
                                    shipLoc += endLoc;
                                    if(isColliding(shipLoc, board))
                                    {
                                            continue;
                                    }
                                    ships[currentShip] = shipLoc;
                                    for(int i = 0; i < shipLoc.length(); i += 2)
                                    {
					String loc = shipLoc.substring(i, i+2);
					board[loc.charAt(0)-97][loc.charAt(1)-48] = 'S';
				}
				currentShip++;
			}
                        

            }
            DisplayShips(board);
            return ships[0]+ships[1]+ships[2]+ships[3]+ships[4];
        }

	private String getShips()
	{
		char[][] board = new char[10][10];
		for(int i=0; i < 10; i++)
		{
			Arrays.fill(board[i], '-');
		}
		System.out.println("Podaj lokalizacje statków które chesz umieścić");
		String[] ships = new String[5];
		int currentShip = 0;
		int length = 0;
		while(currentShip != 5)
		{		
			switch(currentShip)
			{
				case 0:
					System.out.println("Umieszczanie the carrier(5)!"); length = 5; break;
				case 1:
					System.out.println("Umieszczanie battleship(4)!"); length = 4; break;
				case 2:
					System.out.println("Umieszczanie destoryer(3)!"); length = 3; break;
				case 3:
					System.out.println("Umieszczanie submarine(3)!"); length = 3; break;
				case 4:
					System.out.println("Umieszczanie patrol boat(2)!"); length = 2; break;
			}
			DisplayShips(board);
			System.out.println("Podaj pierwszy bok statku");
			String shipLoc = getLine();
			if(checkLoc(shipLoc, board))
			{
				System.out.println("Błedna lokalizacja, konflikt z stniejacym już statkiem");
				continue;
			}
			System.out.println("Podaj końcową pozycje statku");
			String endLoc = getLine();
			if(shipLoc.charAt(0) == endLoc.charAt(0))
			{
				if(shipLoc.charAt(1) - endLoc.charAt(1) < 0)
				{
					if(Math.abs(shipLoc.charAt(1) - endLoc.charAt(1)-1) != length)
					{
						System.out.println("Błedna lokalizacje, niepoprawna długość");
						continue;
					}
					for(int j = 0; j < length-2; j++)
					{
						shipLoc += shipLoc.charAt(0);
						shipLoc += (char)(shipLoc.charAt(1)+j+1);
					}
				}
				else
				{
					if(Math.abs(shipLoc.charAt(1) - endLoc.charAt(1)+1) != length)
					{
						System.out.println("Błedna lokalizacje, niepoprawna długość");
						continue;
					}
					for(int j = 0; j < length-2; j++)
					{
						shipLoc += shipLoc.charAt(0);
						shipLoc += (char)(endLoc.charAt(1)+j+1);
					}
				}
				shipLoc += endLoc;
				if(isColliding(shipLoc, board))
				{
					System.out.println("Błędna lokalizacja, dwa lub więcej statków koliduje ze sobą");
					continue;
				}
				ships[currentShip] = shipLoc;
				for(int i = 0; i < shipLoc.length(); i+=2)
				{
					String loc = shipLoc.substring(i, i+2);
					board[loc.charAt(0)-97][loc.charAt(1)-48] = 'S';
				}
				currentShip++;
				continue;
				
			}
			if(shipLoc.charAt(1) == endLoc.charAt(1))
			{
				if(shipLoc.charAt(0) - endLoc.charAt(0) < 0)
				{
					if(Math.abs(shipLoc.charAt(0) - endLoc.charAt(0)-1) != length)
					{
						System.out.println("Błedna lokalizacje, niepoprawna długość");
						continue;
					}
					for(int j = 0; j < length-2; j++)
					{
						shipLoc += (char)(shipLoc.charAt(0)+j+1); 
						shipLoc += shipLoc.charAt(1);
					}
				}
				else
				{
					if(Math.abs(shipLoc.charAt(0) - endLoc.charAt(0)+1) != length)
					{
						System.out.println("Location invalid, incorrect length specified");
						continue;
					}
					for(int j = 0; j < length-2; j++)
					{
						shipLoc += (char)(endLoc.charAt(0)+j+1);
						shipLoc += shipLoc.charAt(1);
					}
				}
				shipLoc += endLoc;
				if(isColliding(shipLoc, board))
				{
					System.out.println("Location invalid, two or more ships are colliding");
					continue;
				}
				ships[currentShip] = shipLoc;
				for(int i = 0; i < shipLoc.length(); i += 2)
				{
					String loc = shipLoc.substring(i, i+2);
					board[loc.charAt(0)-97][loc.charAt(1)-48] = 'S';
				}
				currentShip++;
				continue;		
			}
			System.out.println("Location invalid, ships must be placed in streight lines");
		}
		return ships[0]+ships[1]+ships[2]+ships[3]+ships[4];
	}

	private boolean checkLoc(String Loc, char[][] board)
	{
		return board[Loc.charAt(0)-97][Loc.charAt(1)-48] == 'S';
	}

	private boolean isColliding(String shipLoc, char[][] board)
	{
		for(int i = 0; i < shipLoc.length(); i += 2)
		{
			String loc = shipLoc.substring(i, i+2);
			if(checkLoc(loc, board))
			{
				return true;
			}
		}
		return false;
	}

	private void DisplayShips(char[][] board)
	{
		String out = " abcdefghij\n";
		for(int i = 0; i < 10; i++)
		{
			out += i;
			for(int j= 0; j < 10; j++)
			{
				out += board[j][i];
			}
			out += '\n';
		}
		System.out.println(out);
	}

	private void ProcessCommand(String Command)
	{
//                System.out.println("Otrzymałem komende "+ Command);
		String internal = Command.substring(0, Command.indexOf(":"));
		String external = Command.substring(Command.indexOf(":")+1);
		if(internal.equals("win")||internal.equals("lose")||internal.equals("reset"))
		{
			activeGame = false;
		}
		System.out.println(external);
	}
}

class ClientNetWorkIO
{
	DatagramSocket clientSocket;
	int port;
        int localport;
	DatagramPacket Packet;
	InetAddress ServerIPAddress;
	byte[] Data;

	ClientNetWorkIO(String address, int port)
	{
		this.port = port;
		try
		{
			ServerIPAddress = InetAddress.getByName(address);
		}
		catch(UnknownHostException e)
		{
			System.err.println("Error: Niepoprawny host");
			System.exit(1);
		}
		try
		{
			clientSocket = new DatagramSocket();
                        this.localport = clientSocket.getLocalPort();
		}
		catch(SocketException e)
		{
			System.err.println("Error: Nie można stworzyc gniazda sieciowego");
			System.exit(1);
		}
	}

	public String getMessage()
	{
		byte[] data = new byte[1024];
		DatagramPacket packet  = new DatagramPacket(data, data.length);
		try
		{
			clientSocket.receive(packet);
		}
		catch(IOException e)
		{
			System.err.println("Error: błąd podczas odbierania danych");
			return "";
		}
		return new String(packet.getData(),0,packet.getLength());
	}

	public void sendMessage(String line)
	{
		Data = line.getBytes();
		DatagramPacket packet = new DatagramPacket(Data, Data.length, ServerIPAddress, port);
		try
		{
			clientSocket.send(packet);
		}
		catch(IOException e)
		{
			System.err.println("Error: bład podczas wysyłania wiadomosci");
		}
	}
}