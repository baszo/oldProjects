/**
  * Created by andrzej2 on 08.03.16.
  */

class Person(firstName: String)
{
  def this(firstName: String, lastName: String) {
    this(firstName)
    println("aaa")
  }

  def talk() = println("talk")
}

val bob = new Person("aaa")
val joe = new Person("aaaa","bbbb")
