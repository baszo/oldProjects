package medistor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrzej2 on 06.02.16.
 */

//Mediator abstrakcyjny
public interface ChatMediator {
    public void SendMessage(String msg, User user);
    void addUser(User user);
}


class ChatMediatorImpl implements ChatMediator{

    List<User> users;

    public ChatMediatorImpl() {
        this.users = new ArrayList<>();
    }

    @Override
    public void SendMessage(String msg, User user) {
        for(User u : this.users){
            //message should not be received by the user sending it
            if(u != user){
                u.receive(msg);
            }
        }
    }

    @Override
    public void addUser(User user) {
        this.users.add(user);
    }
}
