package dekorator;

/**
 * Created by andrzej2 on 03.02.16.
 */
public class Science extends GirlDecorator {

    public Science(Girl girl) {
        super(girl);
    }

    @Override
    public String getDescription() {
        return girl.getDescription() + "+Like Science";
    }

    public void caltulateStuff() {
        System.out.println("scientific calculation!");
    }
}
