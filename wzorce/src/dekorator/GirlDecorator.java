package dekorator;

/**
 * Created by andrzej2 on 03.02.16.
 */
public class GirlDecorator extends Girl {
    protected Girl girl;

    public GirlDecorator(Girl girl) {
        this.girl = girl;
    }

    public String getDescription() {
        return girl.getDescription();
    }
}
