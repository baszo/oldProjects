package dekorator;

/**
 * Created by andrzej2 on 03.02.16.
 */
public class Art extends GirlDecorator {
    public Art(Girl girl) {
        super(girl);
    }
    public String getDescription()
    {
        return girl.getDescription() + "+Like Art";
    }
    public void draw() {
        System.out.println("draw pictures!");
    }
}
