package prototyp;

/**
 * Created by andrzej2 on 06.02.16.
 */
public class Cookie implements Cloneable  {
    public Object clone() {
        try {
            Cookie copy = (Cookie) super.clone();
            return copy;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;

        }
    }
}


class CoconutCookie extends Cookie{}

class CookieMachine {

    private Cookie cookie;//could have been a private Cloneable cookie;

    public CookieMachine(Cookie cookie) {
        this.cookie = cookie;
    }
    public Cookie makeCookie() {
        return (Cookie)cookie.clone();
    }
    public static void main(String args[]) {
        Cookie tempCookie =  null;
        Cookie prot = new CoconutCookie();
        CookieMachine cm = new CookieMachine(prot);
        for (int i=0; i<100; i++)
            tempCookie = cm.makeCookie();
    }
}
