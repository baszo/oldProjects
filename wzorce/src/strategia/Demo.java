package strategia;

import java.util.Scanner;

/**
 * Created by andrzej2 on 02.02.16.
 */
public class Demo {
    public static void main(String[]args){
        Scanner in = new Scanner(System.in);
        String job = in.nextLine();

        try {
            Worker_Context worker = new Worker_Context(job);
            worker.doWork();
        }catch(Exception e){
            System.out.println("no such job  ");
            e.printStackTrace();
        }
    }
}
