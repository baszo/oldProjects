package strategia;

/**
 * Created by andrzej2 on 02.02.16.
 */
public class StrategyCarFix  implements Work{

    @Override
    public void work() {
        System.out.println("Work: Fix car");
    }
}

class StrategyHandleMail implements Work {
    @Override
    public void work() {
        System.out.println("Work: Handle Mail");
    }
}

class StrategyHeal implements Work {
    @Override
    public void work() {
        System.out.println("Work: Heal");
    }
}
