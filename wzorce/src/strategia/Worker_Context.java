package strategia;

/**
 * Created by andrzej2 on 02.02.16.
 */
public class Worker_Context {
    private Work work;

    public Worker_Context(String job) {
        switch (job)
        {
            case "Mailman": work = new StrategyHandleMail(); break;
            case "Doctor": work= new StrategyHeal(); break;
            case "Mechanic":work=new StrategyCarFix();break;
        }
    }

    public void doWork(){
        work.work();
    }
}
