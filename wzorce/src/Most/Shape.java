package Most;

/**
 * Created by andrzej2 on 04.02.16.
 */
public abstract class Shape {
    DrewApi drewapi;

    public Shape(DrewApi drewapi) {
        this.drewapi = drewapi;
    }
    public abstract void drew();
}


class Circle extends Shape{

    private int x, y, radius;

    public Circle(int x, int y, int radius, DrewApi drawAPI) {
        super(drawAPI);
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public void drew() {
        drewapi.drowCircle(radius,x,y);
    }
}
