package Most;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class Demo {
        public static void main(String[] args) {
            Shape redCircle = new Circle(100,100, 10, new RedCircle());
            Shape greenCircle = new Circle(100,100, 10, new GreenCircle());

            redCircle.drew();
            greenCircle.drew();
        }
}
