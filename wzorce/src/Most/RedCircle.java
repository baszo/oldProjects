package Most;

import javafx.scene.shape.Circle;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class RedCircle implements DrewApi {
    @Override
    public void drowCircle(int radius, int x, int y) {
        System.out.println("Drawing Circle[ color: red, radius: " + radius + ", x: " + x + ", " + y + "]");
    }
}
