package Most;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class GreenCircle implements DrewApi {
    @Override
    public void drowCircle(int radius, int x, int y) {
        System.out.println("Drawing Circle[ color: green, radius: " + radius + ", x: " + x + ", " + y + "]");
    }
}
