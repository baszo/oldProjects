package odwiedzający;

/**
 * Created by andrzej2 on 05.02.16.
 */
public class Demo {
    public static void main(String[] args) {
        ItemElementInterface[] items = new ItemElementInterface[]{new Book(20, "1234"),new Book(100, "5678"),
                new Fruit(10, 2, "Banana"), new Fruit(5, 5, "Apple")};

        int total = calculatePrice(items);
        System.out.println("Total Cost = "+total);
    }

    private static int calculatePrice(ItemElementInterface[] items) {
        ShoppingCartVisitor visitor = new ShoppinCartVisitorImpl();
        int sum=0;
        for(ItemElementInterface item : items){
            sum = sum + item.accept(visitor);
        }
        return sum;
    }
}
