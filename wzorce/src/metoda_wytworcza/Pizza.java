package metoda_wytworcza;

/**
 * Created by andrzej2 on 04.02.16.
 */
public abstract class Pizza {
    public abstract double getPrice();
}

class HamAndMashroomPizza extends Pizza{
    private double price = 8.5;

    @Override
    public double getPrice() {
        return price;
    }
}

class Peperoni extends Pizza{
    private double price = 10;

    @Override
    public double getPrice() {
        return price;
    }
}

class Margarita extends Pizza{
    private double price = 12;

    @Override
    public double getPrice() {
        return price;
    }
}
