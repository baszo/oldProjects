package metoda_wytworcza;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class PizzaFactory {
    public enum PizzaType {
        HamAndMashroomPizza,
                Margarita,
                Peperoni
    }

    public static Pizza createPizza(PizzaType pizzaType)
    {
        switch (pizzaType)
        {
            case HamAndMashroomPizza:return new HamAndMashroomPizza();
            case Margarita:return new Margarita();
            case Peperoni:return new Peperoni();
        }
        throw new IllegalArgumentException("Pizza type uknown");
    }
}
