package polecenie;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class Player {
    String id;

    public Player(String id) {
        this.id = id;
    }

    public void startRun(){
        System.out.println("zawodnik " + id + " biega" );
    }

    public void stopRun(){
        System.out.println("zawodnik " + id + " przestal biegac" );
    }

    public void startSwim(){
        System.out.println("zawodnik " + id + " plywa" );
    }

    public void stopSwim(){
        System.out.println("zawodnik " + id + " przestal plywac" );
    }

    public void startExcercies(){
        System.out.println("zawodnik " + id + " cwiczy" );
    }

    public void stopExcersise(){
        System.out.println("zawodnik " + id + " przestal cwiczyc" );
    }
}
