package polecenie;

/**
 * Created by andrzej2 on 04.02.16.
 */
public interface Command {
    public void execute();
    public void undo();
}

class Run implements Command{
    Player player;

    public Run(Player player) {
        this.player = player;
    }

    @Override
    public void execute() {
        player.startRun();
    }

    @Override
    public void undo() {
        player.stopRun();
    }
}

class Swim implements Command{
    Player player;

    public Swim(Player player) {
        this.player = player;
    }

    @Override
    public void execute() {
        player.startSwim();
    }

    @Override
    public void undo() {
        player.stopSwim();
    }
}

class Excercies implements Command{
    Player player;

    public Excercies(Player player) {
        this.player = player;
    }

    @Override
    public void execute() {
        player.startExcercies();
    }

    @Override
    public void undo() {
        player.stopExcersise();
    }
}

class FullWorkout implements Command{
    Command[] tab;

    public FullWorkout( Command[]tab){
        this.tab = tab;
    }

    public void execute(){
        for(Command temp : tab){
            temp.execute();
        }
    }

    public void undo(){
        for(Command temp : tab){
            temp.undo();
        }
    }
}
