package polecenie;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class Coach {
    private Command command;

    public void setMode(Command command) {
        this.command = command;
    }

    public void doExercise()
    {
        command.execute();
    }
    public void undoExecute()
    {
        command.undo();
    }
}
