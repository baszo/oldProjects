package polecenie;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class Demo {
    public static void main(String[]args){
        Coach trener = new Coach();
        Player z1 = new Player("Kowalski");
        Player z2 = new Player("Nowak");
        Player z3 = new Player("Brzeczyszczykiewicz");

        Run bieganie = new Run(z1);
        Swim plywanie = new Swim(z2);

        trener.setMode(bieganie);
        trener.doExercise();
        trener.setMode(plywanie);
        trener.doExercise();
        trener.undoExecute();

        System.out.println();

        Command []tab = { new Run(z3), new Swim(z3), new Excercies(z3)};
        FullWorkout pelnyTrening = new FullWorkout(tab);
        trener.setMode(pelnyTrening);
        trener.doExercise();
    }
}
