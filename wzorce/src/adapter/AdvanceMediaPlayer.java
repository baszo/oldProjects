package adapter;

import strategia.StrategyCarFix;

/**
 * Created by andrzej2 on 06.02.16.
 */
public interface AdvanceMediaPlayer {
    public void playVlc(String fileName);
    public void playMp4(String fileName);
}
