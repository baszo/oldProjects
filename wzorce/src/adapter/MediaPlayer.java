package adapter;

/**
 * Created by andrzej2 on 06.02.16.
 */
public interface MediaPlayer {
    public void play(String audioType, String fileName);
}
