package Iterator;

import java.nio.channels.Channel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrzej2 on 05.02.16.
 */
public class ChannelCollectionImpl implements ChannelCollection {
    private List<Iterator.Channel> channelList;

    public ChannelCollectionImpl(){
        channelList = new ArrayList<>();
    }

    @Override
    public void addChannel(Iterator.Channel c) {
        channelList.add(c);

    }

    @Override
    public void removeChannel(Iterator.Channel c) {
        channelList.remove(c);
    }

    @Override
    public ChannelIterator iterator(ChannelTypeEnum type) {
        return new ChannelIteratorImpl(type,this.channelList);
    }

    private class ChannelIteratorImpl implements ChannelIterator{

        private ChannelTypeEnum type;
        private List<Iterator.Channel> channels;
        private int position;

        public ChannelIteratorImpl(ChannelTypeEnum type, List<Iterator.Channel> channels) {
            this.type = type;
            this.channels = channels;
        }

        @Override
        public boolean hasNext() {
            while (position<channels.size()){
                Iterator.Channel c = channels.get(position);
                if(c.getTYPE().equals(type) || type.equals(ChannelTypeEnum.ALL))
                    return true;
                else
                    position++;
            }
            return false;
        }

        @Override
        public Iterator.Channel next() {
            Iterator.Channel c = channels.get(position);
            position++;
            return c;
        }
    }
}
