package Iterator;

import java.nio.channels.Channel;

/**
 * Created by andrzej2 on 05.02.16.
 */
public interface ChannelIterator {
    public boolean hasNext();
    public Iterator.Channel next();
}
