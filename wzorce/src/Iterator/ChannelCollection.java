package Iterator;

import java.nio.channels.Channel;

/**
 * Created by andrzej2 on 05.02.16.
 */
public interface ChannelCollection {
    public void addChannel(Iterator.Channel c);
    public void removeChannel(Iterator.Channel c);
    public ChannelIterator iterator(ChannelTypeEnum type);
}
