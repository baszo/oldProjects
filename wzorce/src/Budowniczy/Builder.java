package Budowniczy;

import java.util.Scanner;

/**
 * Created by andrzej2 on 05.02.16.
 */
public abstract class Builder {
    protected ZestawKomputerowy zestawKomputerowy;

    public void newZestaw(){
        zestawKomputerowy = new ZestawKomputerowy();
    }

    public ZestawKomputerowy getZestaw(){
        return zestawKomputerowy;
    }

    public abstract void buildMonitor();
    public abstract void buildProcesor();
    public abstract void buildGrafika();
    public abstract void buildRam();
    public abstract void buildHdd();
}

class ConcreteBuilderXT001 extends Builder{
    public void buildMonitor(){
        zestawKomputerowy.setMonitor("Benq 19");
    }

    public void buildProcesor(){
        zestawKomputerowy.setProcesor("amd");
    }

    public void buildGrafika(){
        zestawKomputerowy.setGrafika("ATI");
    }

    public void buildRam(){
        zestawKomputerowy.setRam("DDR3");
    }

    public void buildHdd(){

        Scanner in = new Scanner(System.in);

        int t;
        while(true){
            System.out.println("Dysk do wyboru: (1) Samsung, (2) Segate, (3) Caviar");
            t = in.nextInt();
            if(t>0 && t<4) break;
        }

        String wynik="";
        if(t==1) wynik = "Samsung";
        else if(t==2) wynik = "Segate";
        else if(t==3) wynik = "Caviar";

        zestawKomputerowy.setHdd(wynik);

    }

}

class ConcreteBuilderZestawABC996 extends Builder {

    public void buildMonitor(){
        zestawKomputerowy.setMonitor("LG");
    }

    public void buildProcesor(){
        zestawKomputerowy.setProcesor("INTEL");
    }

    public void buildGrafika(){
//zestaw nie obejmuje karty graficznej
    }

    public void buildRam(){
        zestawKomputerowy.setRam("DDR");
    }

    public void buildHdd(){
        zestawKomputerowy.setHdd("Samsung");
    }
}
