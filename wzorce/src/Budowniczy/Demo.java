package Budowniczy;

/**
 * Created by andrzej2 on 05.02.16.
 */
public class Demo {
    public static void main(String[]args){

        Director szef = new Director();
        Builder builder = new ConcreteBuilderXT001();
        Builder builder2 = new ConcreteBuilderZestawABC996();

        System.out.println("\nZESTAW1");
        szef.setBuilder(builder);
        szef.skladaj();
        ZestawKomputerowy zestaw1 = szef.getZestaw();


        szef.setBuilder(builder2);
        szef.skladaj();
        ZestawKomputerowy zestaw2 = szef.getZestaw();


        zestaw1.show();
        System.out.println("\n\nZESTAW2");
        zestaw2.show();
    }
}
