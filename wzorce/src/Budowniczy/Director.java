package Budowniczy;

/**
 * Created by andrzej2 on 05.02.16.
 */
public class Director {
    private Builder builder;

    public void setBuilder(Builder builder)
    {
        this.builder=builder;
    }

    public void skladaj(){
        builder.newZestaw();
        builder.buildMonitor();
        builder.buildProcesor();
        builder.buildHdd();
        builder.buildRam();
        builder.buildGrafika();
    }

    public ZestawKomputerowy getZestaw(){
        return builder.getZestaw();
    }
}
