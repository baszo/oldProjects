package fabryka_abstrakcyjna;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class PC extends Computer {
    FabricComponentsPc fabricComponentsPc;
    public PC(FabricComponentsPc fabricComponentsPc) {
        this.fabricComponentsPc = fabricComponentsPc;
        make();
    }

    @Override
    public void make() {
        disc = fabricComponentsPc.createDisck().giveDisc();
        ram = fabricComponentsPc.createRam().giveRam();
        processor = fabricComponentsPc.createProcesor().giveProcesor();

    }
}
