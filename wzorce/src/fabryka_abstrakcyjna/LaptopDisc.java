package fabryka_abstrakcyjna;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class LaptopDisc implements Disc {
    String name = "Laptop Disc";

    public Disc giveDisc() {
        System.out.println("Add: " + name);
        return new LaptopDisc();
    }
}
