package fabryka_abstrakcyjna;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class LaptopRam implements Ram {
    String name = "Laptop Disc";

    public Ram giveRam() {
        System.out.println("Add: " + name);
        return new LaptopRam();
    }
}
