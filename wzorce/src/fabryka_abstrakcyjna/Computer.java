package fabryka_abstrakcyjna;

/**
 * Created by andrzej2 on 04.02.16.
 */
public abstract class Computer {
    Disc disc;
    Ram ram;
    Processor processor;

    public abstract void make();

    public void instalowanieOprogramowania(){
        System.out.println("Instaluje oprogramowanie...");
    }

    public void pakowanie(){
        System.out.println("Pakuje sprzet...");
    }
    public void sprzedawanie(){
        System.out.println("Sprzedaje sprzet...");
    }
}
