package fabryka_abstrakcyjna;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class PcProcessor implements Processor {
    String name = "ProcessorForPC";
    public Processor giveProcesor(){
        System.out.println("Add: " + name);
        return new PcProcessor();
    }
}
