package fabryka_abstrakcyjna;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class Laptop extends Computer {
    FabricComponentsLaptop fabricComponentsLaptop;
    public Laptop(FabricComponentsLaptop fabricComponentsLaptop) {
        this.fabricComponentsLaptop = fabricComponentsLaptop;
        make();
    }

    @Override
    public void make() {
        disc = fabricComponentsLaptop.createDisck().giveDisc();
        ram = fabricComponentsLaptop.createRam().giveRam();
        processor = fabricComponentsLaptop.createProcesor().giveProcesor();

    }
}
