package fabryka_abstrakcyjna;

/**
 * Created by andrzej2 on 04.02.16.
 */
public interface FabrycComputerParts {
    public Disc createDisck();
    public Ram createRam();
    public Processor createProcesor();
}
