package fabryka_abstrakcyjna;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class ComputerFabric {
    public Computer giveComputer(String model)
    {
        Computer computer = createComputer(model);
        computer.instalowanieOprogramowania();
        computer.pakowanie();
        computer.sprzedawanie();
        return computer;
    }

    protected Computer createComputer(String model){
        Computer computer = null;
        if(model.equalsIgnoreCase("PC")){
            computer =  new PC(new FabricComponentsPc());
        } else if(model.equalsIgnoreCase("Laptop")){
            computer = new Laptop(new FabricComponentsLaptop());
        }
        return computer;
    }
}
