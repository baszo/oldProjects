package fabryka_abstrakcyjna;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class LaptopProcessor implements Processor {
    String name = "Laptop Processor";

    public Processor giveProcesor() {
        System.out.println("Add: " + name);
        return new LaptopProcessor();
    }
}
