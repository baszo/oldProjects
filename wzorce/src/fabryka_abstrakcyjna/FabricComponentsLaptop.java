package fabryka_abstrakcyjna;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class FabricComponentsLaptop implements FabrycComputerParts {
    @Override
    public Disc createDisck() {
        return new LaptopDisc();
    }

    @Override
    public Ram createRam() {
        return new LaptopRam();
    }

    @Override
    public Processor createProcesor() {
        return new LaptopProcessor();
    }
}
