package fabryka_abstrakcyjna;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class Demo {
    public static void main(String[]args){
        ComputerFabric fabrykaKomputerow = new ComputerFabric();
        System.out.println("PC");
        Computer pc = fabrykaKomputerow.giveComputer("PC");
        System.out.println("\n\nLaptop");
        Computer laptop = fabrykaKomputerow.giveComputer("Laptop");
    }
}
