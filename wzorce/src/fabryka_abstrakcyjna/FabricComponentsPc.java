package fabryka_abstrakcyjna;

/**
 * Created by andrzej2 on 04.02.16.
 */
public class FabricComponentsPc implements FabrycComputerParts {
    @Override
    public Disc createDisck() {
        return new PcDisc();
    }

    @Override
    public Ram createRam() {
        return new PcRam();
    }

    @Override
    public Processor createProcesor() {
        return new PcProcessor();
    }
}
