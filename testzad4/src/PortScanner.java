import java.net.*;
 
class PortScanner {
   public static void main(String []args) throws SocketException, UnknownHostException {
       
       String ip = args[0];
       String protocol = args[1];
       int portmin = Integer.parseInt(args[2]);
       int portmax = Integer.parseInt(args[3]);
       int tmp=0;
       if(portmax < portmin)
       {
           tmp = portmin;
           portmin = portmax;
           portmax = tmp;
       }
       if(protocol.equals("TCP"))
       {
       
            for (int port = portmin; port <= portmax; port++) {
               try {
                  Socket socket = new Socket();
                  socket.connect(new InetSocketAddress(ip, port), 3000);
                  socket.close();
                  System.out.println("Port " + port + " jest otwarty");
              } catch (Exception ex) {
      //            System.out.println(ex.getMessage());
              }
            }
       }
       if(protocol.equals("UDP"))
       {
           DatagramSocket socket = new DatagramSocket();
           InetAddress address = InetAddress.getByName(ip);
           socket.setSoTimeout(1000);
           for (int port = portmin; port <= portmax; port++) {

            
            try{
                socket.connect(address, port);
                socket.send(new DatagramPacket("test".getBytes(), "test".getBytes().length));
                
                while (true)
                {
                    byte[] receive = new byte[4096];
                    DatagramPacket dp = new DatagramPacket(receive, 4096);
                    socket.receive(dp);
                    if (dp != null && dp.getData() != null)
                    {
                        System.out.println("Port " + port + " jest otwarty");
//                        System.out.println(new String(dp.getData(), 0, dp.getLength()));
    //                    logger.info(new String(dp.getData()));
    //                    byte[] bs = dp.getData();
    //                    for (int i = 0; i < bs.length; i++)
    //                    {
    //                        logger.info(bs[i] + "");
    //                    }

                        break;
                    }
                }
                socket.close();
            }
            catch (Exception ex) {
      //            System.out.println(ex.getMessage());
//                System.out.println(ex.getMessage());
                
              }
            
           }
           
       }
       
   }
}
 

